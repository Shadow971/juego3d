﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialMenuController : MonoBehaviour
{
    public Transform parent;
    public GameObject exitButton;
    private RectTransform[] primaryIcons;
    private Vector2[] activePos;

    private int numChilds;

    public int distance;
    public float time = 0.2f;

    public Text text;

    public static RadialMenuController instance;

    Vector2[] newPos;

    void Awake()
    {
        instance = this;
        numChilds = parent.childCount;
        primaryIcons = new RectTransform[numChilds];

        int offset = 360 / numChilds;

        activePos = new Vector2[numChilds];
        newPos = new Vector2[numChilds];

        for (int i = 0; i < numChilds; i++)
        {
            float angle = (offset * i) * Mathf.Deg2Rad;

            activePos[i].x = distance * Mathf.Cos(angle);
            activePos[i].y = distance * Mathf.Sin(angle);

            primaryIcons[i] = parent.GetChild(i).GetComponent<RectTransform>();
        }
    }
    public void Open()
    {
        PlayerFollow.instance.SetMovement(false);
        exitButton.SetActive(true);

        for (int i = 0; i < numChilds; i++)
        {
            primaryIcons[i].localPosition = activePos[i];
        }
        
    }
    public void Open(GameObject go, GameObject parent)
    {
        go.SetActive(true);
        Vector3 dir = parent.GetComponent<RectTransform>().localPosition;

        float goAngle = FindDegree(dir.y, dir.x);
        int numChilds = go.transform.childCount;
        int offset = 20;
        int dis = distance + 60;
        int middle = offset * (numChilds - 1) / 2;
        for (int i = 0; i < numChilds; i++)
        {
            float angle = offset * i - middle;
            angle += goAngle;
            angle *= Mathf.Deg2Rad;

            Vector2 pos;
            pos.x = dis * Mathf.Cos(angle);
            pos.y = dis * Mathf.Sin(angle);

            GameObject child = go.transform.GetChild(i).gameObject;
            child.GetComponent<RectTransform>().localPosition = pos;
        }
    }
    public void Close()
    {
        PlayerFollow.instance.SetMovement(true);
        exitButton.SetActive(false);
        //StartCoroutine(Disable(exitButton));
    }
    public void Close(GameObject go)
    {
        go.SetActive(false);
        //StartCoroutine(Disable(go));
    }
    public void ChangeText(string s)
    {
        text.text = s;
    }
    private float FindDegree(float y, float x)
    {
        float value = Mathf.Atan2(y, x) * Mathf.Rad2Deg;
        if (value < 0)
        {
            value += 360f;
        }
        return value;
    }

    private IEnumerator Disable(GameObject go)
    {
        yield return new WaitForSeconds(time);
        go.SetActive(false);
    }
}
