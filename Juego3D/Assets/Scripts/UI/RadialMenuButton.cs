﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RadialMenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject folder;
    public void OnPointerEnter(PointerEventData eventData)
    {
        RadialMenuController.instance.Open(folder, gameObject);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        
    }
}
