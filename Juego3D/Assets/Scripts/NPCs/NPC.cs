﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC
{
    public GameObject character;
    public List<Quest> quest = new List<Quest>();
    public string name;

    public NPC(GameObject go, string n)
    {
        character = go;
        name = n;
    }
}
