﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcController : MonoBehaviour
{
    private bool playerNear;
    private bool talkStarted;

    private NPC npc;
    public string npcName = "Juana";

    public string[] sentences;
    private KeyCode[] keyCodes = new KeyCode[] {KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5};
    private void Start()
    {
        npc = new NPC(gameObject, npcName);
        NpcManager.instance.Suscribe(npc);
    }

    private void Update()
    {
        if(Input.anyKeyDown)
        {
            bool can = DialogueSystem.instance.CanAction();
            if (playerNear && can)
            {
                if (Input.GetKeyDown(KeyCode.E) && !talkStarted)
                {
                    StartDialogue();
                }
                else if (Input.GetKeyDown(KeyCode.Escape))
                {
                    EndDialogue();
                }
                else if (talkStarted)
                {
                    Interact();
                }
            }
        }
    }

    /// <summary>
    /// Checks if a number has been pressed
    /// </summary>
    private void Interact()
    {
        for (int i = 0; i < keyCodes.Length; i++)
        {
            if (Input.GetKeyDown(keyCodes[i]))
            {
                CheckOption(i);
            }
        }
    }

    private void StartDialogue()
    {
        talkStarted = true;
        string s = "¿Need help?\n";

        DialogueSystem.instance.SetNPC(this);
        DialogueSystem.instance.ShowDialogue(npcName, s, QuestToOption());
        MovementController.instance.SetMovement(false);
        PlayerFollow.instance.SetMovement(false);
    }

    private string[] QuestToOption()
    {
        string[] op = new string[npc.quest.Count];
        for (int i = 0; i < npc.quest.Count; i++)
        {
            op[i] = (i + 1) + ".- Complete " + npc.quest[i].QuestName;
        }

        return op;
    }

    private void EndDialogue()
    {
        DialogueSystem.instance.FinishDialogue();
        MovementController.instance.SetMovement(true);
        PlayerFollow.instance.SetMovement(true);
        talkStarted = false;
    }

    public void CheckOption(int index)
    {
        if (index == npc.quest.Count)
        {
            EndDialogue();
        }
        else if (index < npc.quest.Count)
        {
            Quest q = npc.quest[index];
            if (q != null)
            {
                if (q.Completed)
                {
                    q.GiveReward();
                    npc.quest.Remove(q);

                    string sentence = "Thank you! Take " + q.ItemReward.amount + " " + q.ItemReward.name;
                    DialogueSystem.instance.UpdateDialogue(sentence, QuestToOption());
                }
                else
                {
                    DialogueSystem.instance.UpdateDialogue(q.QuestName + " is not completed!");
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            playerNear = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerNear = false;
        }
    }
}
