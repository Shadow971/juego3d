﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour
{
    public Transform player;
    public Transform editTarget;
    
    private float inputSensitivity = 150.0f;
    public float clampAngle = 80.0f;

    private float rotY = 0.0f;
    private float rotX = 0.0f;

    private bool canMove = true;

    public float springK = 10.0f;
    public float damping = 5.0f;
    private Vector3 idealPos;
    private Vector3 cameraVel = new Vector3(0.0f, 0.0f, 0.0f);

    public static PlayerFollow instance;
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;

        if(player == null)
        {
            MovementController m = FindObjectOfType<MovementController>();
            player = m.transform;
        }
        idealPos = player.position;

        SetMovement(true);
    }

    void Update()
    {
        float dt = Time.deltaTime;
        if(canMove)
        {
            Rotation(dt);
        }

        CameraUpdater(dt);
    }

    private void Rotation(float dt)
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        rotY -= mouseX * inputSensitivity * dt;
        rotX += mouseY * inputSensitivity * dt;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        Quaternion localRotation = Quaternion.Euler(-rotX, -rotY, 0.0f);
        transform.rotation = localRotation;
    }

    void CameraUpdater(float dt)
    {
        Vector3 pos = transform.position;
        if (InputManager.instance.editing)
        {
            idealPos = editTarget.position;
        }
        else
        {
            idealPos = player.position;
        }

        Vector3 offset = idealPos - pos;
        float damp = Mathf.Min(1.0f, damping * dt);
        cameraVel *= 1.0f - damp;
        Vector3 springAccel = offset * springK;
        cameraVel += springAccel * dt;

        Vector3 movement = cameraVel * dt;
        transform.position += movement;
    }
    public void SetMovement(bool b)
    {
        canMove = b;
        if (!canMove)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}