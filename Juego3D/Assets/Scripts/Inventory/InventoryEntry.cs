﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryEntry : MonoBehaviour
{
    Image image;
    Text  nameText;
    Text  description;
    Text  amount;

    Text  sellAmount;
    bool  isSelling;

    // Start is called before the first frame update
    void Start()
    {
        image = transform.GetChild(0).GetComponent<Image>();
        nameText = transform.GetChild(1).GetComponent<Text>();
        description = transform.GetChild(2).GetComponent<Text>();
        amount = transform.GetChild(3).GetComponent<Text>();
        sellAmount = transform.GetChild(4).GetComponent<Text>();
    }


}
