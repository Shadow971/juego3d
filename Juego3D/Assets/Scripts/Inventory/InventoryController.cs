﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryItem
{
    public string name;
    private string description;
    public Sprite image;

    public int inventoryAmount;

    public InventoryItem(string name, string description, Sprite image)
    {
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public int GetInventoryAmount() { return inventoryAmount; }
    public void AddAmount(int cant) { inventoryAmount += cant; }
    public void SubstractAmount(int cant) { inventoryAmount -= cant; }
}
public class InventoryController : MonoBehaviour
{
    #region ExternalScripts
    QuestController missions;
    private FeedController feed;

    public static InventoryController Instance;     //Singleton
    #endregion

    #region Inventory Array
    InventoryItem[] materiales = new InventoryItem [3];// Materials inventory
    InventoryItem[] items = new InventoryItem [24]; // Items inventory
    bool[] itemsPlaceAvailable = new bool[24];
    // InventoryItem[] seeds = new Seed [3]; // Seed inventory //lo hizo un Wizard
    int money = 0;
    #endregion

    #region Public External Objects
    [Tooltip("Parent of all tools")] public GameObject tools;
    [Tooltip("Parent of the spin (GUI)")] public GameObject spin;
    [Tooltip("Parent of all materials (GUI)")] public GameObject materialsGUI;
    [Tooltip("Parent of all Inventory Entrys")] public GameObject[] InventoryEntry;
    [Tooltip("Book")] public GameObject book;
    [Tooltip("Delete Panel")] public GameObject deletePanel;
    public Image cursor;
    #endregion

    #region Tools private variables
    private int numItems = 0; //num of items in the inventory
    private int activeTool = 0; // tool that i wanna use
    private int oldActive = 0; // tool active in this moment
    #endregion

    Item itemSelected; //Item selected in the inventory
    private int currentPage = 1; //1.2.3

    private void Awake()
    {
        Instance = this;    //Singleton
    }

    private void Start()
    {
        missions = QuestController.Instance;
        feed = FindObjectOfType<FeedController>();
    }

    private void Update()
    {
        //TODO change panel of menu
        OpenCloseMenu();
    }

    //open and close the menu / inventory
    private void OpenCloseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Tab))  //open Menu
        {
            book.SetActive(!book.activeSelf);
            if (book.activeSelf)
            {
                ChangeGui(currentPage);
                Time.timeScale = 0;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Time.timeScale = 1;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }

    // Inputs and code for change the current tool that is active


    //Add a material to the inventory
    public void AddMaterial( GameObject obj)
    {
        int pos;
        feed.Suscribe(obj.tag, obj.GetComponent<Image>().sprite, obj.GetComponent<Item>().amount); //pop that show player what you have obtain

        Item it = obj.GetComponent<Item>();
        switch (it.name) //TODO change this 
        {
            case "Wood":
                pos = 0;
            break;
            case "Stone":
                pos = 1;
                break;
            default:
                pos = 2;
                break;
        }

       //materiales[pos] += it.amount;
    }

    //Change the buttons, images, etc (Use when next page or other tag in book)
    void ChangeGui(int page)
    {
        for(int i = 0; i < InventoryEntry.Length; i++)
        {
            
        }
    }

    //Add an Item to inventory (add new && add amount)
    public void AddItem(Item newItem)
    {
        //TODO: meter materiales aca
        InventoryItem item = Search(newItem.name);
        if (item != null)
        {
            item.AddAmount(newItem.amount);
        } 
        else if (numItems < items.Length)
        {
            items[numItems] = new InventoryItem(newItem.name, newItem.description, newItem.image);
            item = items[numItems];
            item.AddAmount(newItem.amount);
            numItems++;
        }
        GameEvents.Instance.ItemCollected(item.name, item.GetInventoryAmount());
        feed.Suscribe(item.name, item.image, newItem.amount);

    }
    //Delete item
    public void ConfirmDeleteItem(int pos)
    {
        items[pos] = null;
    }
    public void SubstractAmount(int cant, string id)
    {
        InventoryItem item = Search(id);
        if (item != null)
        {
            item.SubstractAmount(cant);
            GameEvents.Instance.ItemCollected(item.name, item.GetInventoryAmount());
            if (item.inventoryAmount <= 0)
            {
                item = null;
            }
        }
        
    }
    public void SelectItem(int pos)
    {
        //itemSelected = items[pos];
    }
    public void ActiveDeletePanel()
    {
        deletePanel.SetActive(true);
    }

    // Return the current amount in inventory of an item
    public int GetAmount(string name)
    {
        if (name.Equals("money") || name.Equals("Money"))
        {
            return money;
        }
        else
        {
            InventoryItem item = Search(name);
            if (item != null)
            {
                return item.GetInventoryAmount();
            }
                
            else
            {
                Debug.Log("no esta en el inventario compa");
                return 0;
            }      
        }
    }

    //Search for an Item (return pos in array or -1 if there's no in inventory)
    private InventoryItem Search(string name)
    {
        for (int i = 0; i < items.Length; i++)
        {
            if(items[i] != null)
            {
                if (name.Equals(items[i].name))
                    return items[i];
            }
            
        }
        return null;
    }

    //TODO
    //TODO comprar items
    //TODO function that sell items (give item + delete)
    //TODO function that manage money
    //TODO notes in gameplay for missions
    //TODO spin / quick acces

    private void OnTriggerEnter(Collider other)
    {
        ///Player tiene un collider le pasa el script ITEM cuando pase por encima y despues lo destruye
        AddMaterial(other.gameObject);
        Destroy(other.gameObject);
    }
}
