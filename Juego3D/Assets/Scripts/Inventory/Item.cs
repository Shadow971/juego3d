﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Data/Item")]

public class Item : ScriptableObject
{
    public string name;
    public string description;
    public Sprite image;

    public int amount;
}
