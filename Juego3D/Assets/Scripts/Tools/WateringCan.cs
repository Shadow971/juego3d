﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WateringCan : MonoBehaviour
{
    public Transform wateringCan;
    public ParticleSystem waterParticles;

    private Transform oriWaterCanPos;
    private Transform activeWaterCanPos;
    private Vector3 newPos;

    private void Start()
    {
        //oriWaterCanPos.position = wateringCan.localPosition;
        waterParticles.Stop();
    }
    void Update()
    {
        if (InputManager.instance.editing)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //wateringCan.localPosition = activeWaterCanPos.position;
                waterParticles.Play();
            }
            if (Input.GetMouseButtonUp(0))
            {
                //wateringCan.localPosition = oriWaterCanPos.position;
                waterParticles.Stop();
            }
        }
    }
    private void OnEnable()
    {
        waterParticles.Stop();
    }
}
